#include "opcontrol.h"

#include "main.h"
#include "constants.h"
#include "pid.h"
#include "global.h"
#include "autonomous.h"
#include <sstream>
#include <chrono>

Constants* constants = Constants::GetInstance();
auto awning_pid = new Pid(&constants->awning_kp, &constants->awning_ki, &constants->awning_kd);
auto lift_pid = new Pid(&constants->lift_kp, &constants->lift_ki, &constants->lift_kd);


template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 0)
{
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    return out.str();
}
// Display Values on Screen

void LogValues () {
    std::cout << to_string_with_precision(pros::millis(),7) << "," <<
                 to_string_with_precision(left_front_wheels.get_position(),7) << "," <<    
                 to_string_with_precision(left_center_wheels.get_position(),7) << "," <<   
                 to_string_with_precision(left_back_wheels.get_position(),7) << "," << 
                 to_string_with_precision(right_front_wheels.get_position(),7) << "," <<
                 to_string_with_precision(right_center_wheels.get_position(),7) << "," <<   
                 to_string_with_precision(right_back_wheels.get_position(),7) << "," <<    
                 to_string_with_precision(lift_motor.get_position(),7) << "," <<    
                 to_string_with_precision(awning_motor.get_position(),7) << std::endl;
}

void DisplayValues () {
    if (pros::lcd::is_initialized() == false) {
        pros::lcd::initialize();
    }
    pros::lcd::clear_line(1);
    pros::lcd::set_text(1, "Position: " +
                           to_string_with_precision(left_front_wheels.get_position()) + "," +    
                           to_string_with_precision(left_center_wheels.get_position()) + "," +    
                           to_string_with_precision(left_back_wheels.get_position()) + "," +    
                           to_string_with_precision(right_front_wheels.get_position()) + "," +    
                           to_string_with_precision(right_center_wheels.get_position()) + "," +    
                           to_string_with_precision(right_back_wheels.get_position()) + "," +    
                           to_string_with_precision(lift_motor.get_position()) + "," +    
                           to_string_with_precision(awning_motor.get_position()));  
    pros::lcd::clear_line(2);
    pros::lcd::set_text(2, "Velocity: " +
                           to_string_with_precision(left_front_wheels.get_actual_velocity()) + "," +    
                           to_string_with_precision(left_center_wheels.get_actual_velocity()) + "," +    
                           to_string_with_precision(left_back_wheels.get_actual_velocity()) + "," +    
                           to_string_with_precision(right_front_wheels.get_actual_velocity()) + "," +    
                           to_string_with_precision(right_center_wheels.get_actual_velocity()) + "," +    
                           to_string_with_precision(right_back_wheels.get_actual_velocity()) + "," +    
                           to_string_with_precision(lift_motor.get_actual_velocity()) + "," +    
                           to_string_with_precision(awning_motor.get_actual_velocity()));  
    pros::lcd::set_text(6, "Awning Constants: kp - " + std::to_string(*&constants->awning_kp) +
                           ", ki - " + std::to_string(*&constants->awning_ki) + ", kd - " +
                           std::to_string(*&constants->awning_kd));
    pros::lcd::set_text(7, "Lift Constants: kp - " + std::to_string(*&constants->lift_kp) + ", ki - " +
                           std::to_string(*&constants->lift_ki) + ", kd - " +
                           std::to_string(*&constants->lift_kd));
}

// Random Useful Functions

bool soft_stop(bool is_maximum, double stop_position, double current_position) {
	if (is_maximum == true and stop_position >= current_position) {
		return false;
	}
	else if (is_maximum == false and stop_position <= current_position) {
		return false;
	}
	else {
		return true;
	}
}

// Analog
// Drives

void ArcadeDrive() {
    int power = master.get_analog(ANALOG_LEFT_Y);
    int turn = master.get_analog(ANALOG_LEFT_X);
    int left = power + turn;
    int right = power - turn;

    left_front_wheels.move(left);
    left_center_wheels.move(left);
    left_back_wheels.move(left);
    right_front_wheels.move(right);
    right_center_wheels.move(right);
    right_back_wheels.move(right);
}

void SplitArcadeDrive() {
    int power = master.get_analog(ANALOG_LEFT_Y);
    int turn = master.get_analog(ANALOG_RIGHT_X);
    int left = power + turn;
    int right = power - turn;

    left_front_wheels.move(left);
    left_center_wheels.move(left);
    left_back_wheels.move(left);
    right_front_wheels.move(right);
    right_center_wheels.move(right);
    right_back_wheels.move(right);
}

void TankDrive() {
		left_front_wheels.move(master.get_analog(ANALOG_LEFT_Y));
		left_center_wheels.move(master.get_analog(ANALOG_LEFT_Y));
		left_back_wheels.move(master.get_analog(ANALOG_LEFT_Y));
		right_front_wheels.move(master.get_analog(ANALOG_RIGHT_Y));
		right_center_wheels.move(master.get_analog(ANALOG_RIGHT_Y));
		right_back_wheels.move(master.get_analog(ANALOG_RIGHT_Y));
}

// Digital
// Miscellaneous

void AwningPositions() {

}

void Awning() {
	
    if (master.get_digital(DIGITAL_UP)) {
        awning_motor.move_velocity(100);
        awning_setpoint = awning_motor.get_position();
        awning_pid->ResetError();
    } else if (master.get_digital(DIGITAL_LEFT)) {
        awning_motor.move_velocity(-100);
        awning_setpoint = awning_motor.get_position();
        awning_pid->ResetError();
    } else {
        awning_output = awning_pid->Update(awning_setpoint, awning_motor.get_position());
        awning_motor.move_velocity(awning_output);
    }   
	
}

void AwningControl(int awning_pos) {
	if (awning_pos == 0 && awning_motor.get_position() > 0)
		awning_motor.move_velocity(-25);
	else if (awning_pos == 1 && awning_motor.get_position() < 300 + pros::battery::get_capacity()/11)
		awning_motor.move_velocity(100);
	else if (awning_pos == 2) {
		Awning();
	} else
        //awning_output = awning_pid->Update(0, awning_setpoint);
		awning_motor.move_velocity(0);
}

void Lift() {
    if (master.get_digital(DIGITAL_R1)) {
        lift_motor.move_velocity(150);
        lift_setpoint = lift_motor.get_position();
        lift_pid->ResetError();
    } else if (master.get_digital(DIGITAL_R2)) {
        lift_motor.move_velocity(-100);
        lift_setpoint = lift_motor.get_position();
        lift_pid->ResetError();
    } else {
        //lift_output = lift_pid->Update(lift_setpoint, lift_motor.get_position());
        lift_motor.move_velocity(0);
    }   
}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
	int awning_pos = 0; //0 = back, 1 = up, 2 = manual
	int lift_pos = 0; //0 = manual, 1 = flag toggle

    std::uint32_t now = pros::millis();

	while (true) {
        TankDrive();
        Lift();
		AwningPositions();
        DisplayValues();
        LogValues();

		if (master.get_digital(DIGITAL_X)) {
			lift_pos = 1;
        }

		if (lift_pos == 1) {
			if (lift_motor.get_position() < 850)
				lift_motor.move_velocity(100);
			else if (lift_motor.get_position() > 950)
				lift_motor.move_velocity(-100);
			else {
				lift_motor.move_velocity(0);
				lift_pos = 0;
			}
		}

		if (master.get_digital(DIGITAL_L2)) {
			awning_pos = 0;
        }
		if (master.get_digital(DIGITAL_L1)) {
			awning_pos = 1;
        }
		if (master.get_digital(DIGITAL_RIGHT)) {
			awning_motor.tare_position();
        }
		if (master.get_digital(DIGITAL_DOWN)) {
			awning_pos = 2;
        }

		AwningControl(awning_pos);

        pros::Task::delay_until(&now, 20);
	}
}
